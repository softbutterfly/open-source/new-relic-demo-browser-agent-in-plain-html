(function () {
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        M.FormSelect.init(elems);
    });

    var pokedexForm = document.getElementById('pokedex-form');
    var pokedexInfo = document.getElementById('pokedex-info');

    pokedexForm.addEventListener('submit', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var pokemon = pokedexForm.elements['pokemon'].value;

        if (pokemon) {
            fetch('https://pokeapi.co/api/v2/pokemon/' + pokemon)
                .then(
                    function (response) {
                        return response.json();
                    }
                )
                .then(
                    function (data) {
                        pokedexInfo.innerText = JSON.stringify(data, null, 2);
                    }
                );
        }
    });

})();
